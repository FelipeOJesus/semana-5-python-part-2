def insertion_sort(lista):
    for i in range(len(lista)-1):
        indice = i+1
        trocou = True
        while (indice > 0 and trocou):
            if(lista[indice] < lista[indice-1]):
                lista[indice], lista[indice - 1] = lista[indice -1], lista[indice]
            else:
                trocou = False
            indice -= 1
    return lista

def main():
    lista = [9,7,4,2,1,3,5,6,8]
    print(insertion_sort(lista))


main()