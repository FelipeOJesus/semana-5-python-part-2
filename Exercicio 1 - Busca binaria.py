def busca(lista, elemento):
    primeiro = 0
    ultimo = len(lista)-1

    lista.sort()

    while(primeiro <= ultimo):
        meio = (primeiro + ultimo) // 2
        print (meio)
        if(elemento == lista[meio]):
            return meio
        if(elemento < lista[meio]):
            ultimo = meio - 1
        else:
            primeiro = meio + 1
    return False
            
def main():
    print(busca(['a', 'e', 'i'], 'e'))
    '''
    1
    # deve devolver => 1
    '''
    print(busca([1, 2, 3, 4, 5], 6))
    '''
    2
    3
    4
    # deve devolver => False
    '''

    print(busca([1, 2, 3, 4, 5, 6], 4))
    '''
    2
    4
    3
    # deve devolver => 3
    '''

main()